FROM registry.gitlab.com/sbenv/veroxis/images/cargo-wasm-bindgen-cli:0.2.93 as cargo-wasm-bindgen-cli
FROM registry.gitlab.com/sbenv/veroxis/images/cargo-trunk:0.19.3 as cargo-trunk

# ---

FROM registry.gitlab.com/sbenv/veroxis/images/sdks/rust:1.80.1

RUN rustup target add "wasm32-unknown-unknown" && \
    rustup target add "wasm32-unknown-emscripten" && \
    rustup target add "wasm32-wasi" && \
    rustup toolchain add "nightly" && \
    rustup target add "wasm32-unknown-unknown" --toolchain="nightly" && \
    rustup target add "wasm32-unknown-emscripten" --toolchain="nightly" && \
    rustup target add "wasm32-wasi" --toolchain="nightly" && \
    rustup component add "rust-src" --toolchain="nightly" && \
    rustup component add "rustfmt" --toolchain="nightly" && \
    rustup component add "clippy" --toolchain="nightly" && \
    chmod -R g+w,o+w /usr/local/rustup && \
    chmod -R g+w,o+w /usr/local/cargo

COPY --from=cargo-wasm-bindgen-cli "/usr/local/bin/wasm-bindgen" "/usr/local/bin/wasm-bindgen"
COPY --from=cargo-wasm-bindgen-cli "/usr/local/bin/wasm-bindgen-test-runner" "/usr/local/bin/wasm-bindgen-test-runner"
COPY --from=cargo-wasm-bindgen-cli "/usr/local/bin/wasm2es6js" "/usr/local/bin/wasm2es6js"

COPY --from=cargo-trunk "/usr/local/bin/trunk" "/usr/local/bin/trunk"
