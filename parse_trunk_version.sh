#!/bin/sh

sed '/^FROM/!d;/as cargo-trunk/!d' Dockerfile | tail -n 1 | cut -d':' -f2 | cut -d' ' -f1
